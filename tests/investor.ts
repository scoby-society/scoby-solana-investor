import * as anchor from "@project-serum/anchor";
import { Program } from "@project-serum/anchor";
import { Investor } from "../target/types/investor";

import {
  TOKEN_PROGRAM_ID,
  MINT_SIZE,
  createAssociatedTokenAccountInstruction,
  getAssociatedTokenAddress,
  createInitializeMintInstruction,
  getAccount
} from "@solana/spl-token"; 
const { SystemProgram, PublicKey } = anchor.web3;


describe("investor", () => {
  // Configure the client to use the local cluster.
  const provider = anchor.AnchorProvider.env();
  anchor.setProvider(provider);
  
  const program_id = new anchor.web3.PublicKey("E7MsQUaKg44L5gSEw7CRq3SbF2DCbEezpMQ4jMUr6TDu")
  const idl = JSON.parse(
    require("fs").readFileSync("./target/idl/investor.json", "utf8")
  );
  const program = new anchor.Program(idl, program_id);

  // const program = anchor.workspace.Investor as Program<Investor>;
  //2km8YLnQuJ3xxqrgW8XQeXSMxu7GDTE8tnvskpXbaHXowx259y7WfJr83roeQeqDbZaHxNKTzxst85nhhBXNns7C

  // Generate a random keypair that will represent our token
  const mintKey: anchor.web3.Keypair = anchor.web3.Keypair.generate();
  const revenue = 3
  const supply = 100
  // AssociatedTokenAccount for anchor's workspace wallet
  let associatedTokenAccount = undefined;

  let mintAddress;
  

  it("Create Mint Public Key", async () => {
    const key = anchor.AnchorProvider.env().wallet.publicKey;

    const lamports: number = await program.provider.connection.getMinimumBalanceForRentExemption(
      MINT_SIZE
    );
    associatedTokenAccount = await getAssociatedTokenAddress(
      mintKey.publicKey,
      key
    );

    const mint_tx = new anchor.web3.Transaction().add(
      anchor.web3.SystemProgram.createAccount({
        fromPubkey: key,
        newAccountPubkey: mintKey.publicKey,
        space: MINT_SIZE,
        programId: TOKEN_PROGRAM_ID,
        lamports,
      }),
      // Fire a transaction to create our mint account that is controlled by our anchor wallet
      createInitializeMintInstruction(
        mintKey.publicKey, 0, key, key
      ),
      // Create the ATA account that is associated with our mint on our anchor wallet
      createAssociatedTokenAccountInstruction(
        key, associatedTokenAccount, key, mintKey.publicKey
      )
    );

    // sends and create the transaction
    const res = await anchor.AnchorProvider.env().sendAndConfirm(mint_tx, [mintKey]);

 
    console.log("Transaction Details: ", res);
    console.log("Mint key: ", mintKey.publicKey.toString());
    console.log("User: ", key.toString());

    mintAddress = mintKey.publicKey.toString()
  })
  


  it("Create Supply", async () => {
      const mintPublicKey = new anchor.web3.PublicKey(mintAddress);
      const key = anchor.AnchorProvider.env().wallet.publicKey;


      associatedTokenAccount = await getAssociatedTokenAddress(
        mintPublicKey,
        key
      );

      const mintresult = await program.methods.mintToken(new anchor.BN(supply)).accounts({
        mint: mintPublicKey,
        tokenProgram: TOKEN_PROGRAM_ID,
        tokenAccount: associatedTokenAccount,
        authority: key
      }).rpc();

      console.log("mintresult ",mintresult)

  })

  it("Transfer token to investors", async () => {
    // Get anchor's wallet's public key
    const mintPublicKey = new anchor.web3.PublicKey(mintAddress);
    const myWallet = anchor.AnchorProvider.env().wallet.publicKey;
    associatedTokenAccount = await getAssociatedTokenAddress(
      mintPublicKey,
      myWallet
    );


    const user1: anchor.web3.Keypair = anchor.web3.Keypair.generate();
    console.log("user 1",user1.publicKey.toString())

    const associatedTokenAccount1 = await getAssociatedTokenAddress(
      mintPublicKey,
      user1.publicKey
    );

    const user2: anchor.web3.Keypair = anchor.web3.Keypair.generate();
    console.log("user 2",user2.publicKey.toString())
    const associatedTokenAccount2 = await getAssociatedTokenAddress(
      mintPublicKey,
      user2.publicKey
    );

    // Fires a list of instructions
    const mint_tx_user1 = new anchor.web3.Transaction().add(
      // Create the ATA account that is associated with our To wallet
      createAssociatedTokenAccountInstruction(
        myWallet, associatedTokenAccount1, user1.publicKey, mintPublicKey
      )
    );

    // Sends and create the transaction
    await anchor.AnchorProvider.env().sendAndConfirm(mint_tx_user1, []);

    // Fires a list of instructions
    const mint_tx_user2 = new anchor.web3.Transaction().add(
      // Create the ATA account that is associated with our To wallet
      createAssociatedTokenAccountInstruction(
        myWallet, associatedTokenAccount2, user2.publicKey, mintPublicKey
      )
    );

    // Sends and create the transaction
    await anchor.AnchorProvider.env().sendAndConfirm(mint_tx_user2, []);

    // Executes our transfer smart contract 
    await program.methods.transferToken(new anchor.BN(50)).accounts({
      tokenProgram: TOKEN_PROGRAM_ID,
      from: associatedTokenAccount,
      fromAuthority: myWallet,
      to: associatedTokenAccount1,
    }).rpc();

    // Executes our transfer smart contract 
    await program.methods.transferToken(new anchor.BN(50)).accounts({
      tokenProgram: TOKEN_PROGRAM_ID,
      from: associatedTokenAccount,
      fromAuthority: myWallet,
      to: associatedTokenAccount2,
    }).rpc();

  });

  it("Get List and process payment", async () => {
    const mintPublicKey = new anchor.web3.PublicKey(mintAddress);
    const myWallet = anchor.AnchorProvider.env().wallet.publicKey;

    const data = await program.provider.connection.getParsedProgramAccounts(
      TOKEN_PROGRAM_ID,
      {
        filters: [{
          memcmp:{
            offset:0,
            bytes: mintAddress
          }
        }]
      }
    )
    const totalSupplyData = await program.provider.connection.getTokenSupply(mintPublicKey)
    const totalSupply = totalSupplyData.value.uiAmount
    console.log("totalSupply",totalSupply)
    let users = []
    for (let index = 0; index < data.length; index++) {
      const element = data[index];
      const result1 = await getAccount(program.provider.connection,element.pubkey)
      const tokendetails = await program.provider.connection.getParsedTokenAccountsByOwner(result1.owner,{
        mint: mintPublicKey
      })

      if(tokendetails.value[0].account.data.parsed.info.tokenAmount.amount > 0) {
        const balance = tokendetails.value[0].account.data.parsed.info.tokenAmount.amount
        const amount = ((balance/totalSupply) * revenue) * 1000000000
        console.log("amount ", amount)
        const revenueResult = await program.methods.sendRevenue(new anchor.BN(amount)).accounts({
          from: myWallet,
          to:result1.owner,
          systemProgram : SystemProgram.programId
        }).rpc();
        console.log("revenueResult ",revenueResult)
      }
    }
  })


});
