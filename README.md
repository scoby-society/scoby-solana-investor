# Investor



## Anchor Deployment

1. From Terminal run "yarn"
2. Run "anchor build"
3. copy the so file path and run "solana deploy"
4. To run testcases run "anchor test"

## Anchor UI Demo deployment

1. Go to UI folder
2. Run "yarn"
3. update program id, clustor, mint address in env file
4. Run "yarn start"
5. Go to UI/Wallet folder
6. import the wallet into phantom
7. use the wallet to manage mint address "9nqJa23g5jHjz4H8rSkXnFeUJgS4C9R26RRHPZMRqs5G"
