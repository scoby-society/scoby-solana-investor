interface SCSModelTypes {
  MINT: string;
  TRANSFER: string;
  BURN: string;
  SEND: string;
}
const SCSModelTypesValue: SCSModelTypes = {
  MINT: 'mint',
  TRANSFER: 'transfer',
  BURN: 'burn',
  SEND: 'send'
};

export default SCSModelTypesValue;
