import React from 'react';
import HashLoader from 'react-spinners/HashLoader';

function SCSLoaderVW(props: any) {
  return (
    <div className="ground-container-inner">
      <div className="nodata">
        <div className="loader-container">
          <HashLoader />
        </div>

        <p>{props.title}</p>
      </div>
    </div>
  );
}
export default SCSLoaderVW;
