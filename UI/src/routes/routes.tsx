import SCSHomeVC from '../components/SCSHomeVC';

const publicRoutes = [
  {
    path: '/',
    exact: true,
    component: SCSHomeVC
  }
];

export default publicRoutes;
