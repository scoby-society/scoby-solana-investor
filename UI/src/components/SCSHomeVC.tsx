import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import Logo from '../assets/images/logo.png';
import SCSModelTypesValue from '../types/SCSModelTypes';
import SCSConfig from '../helpers/SCSConfig';
import PhantomProvider from '../types/SCSProviderTypes';
import { toast } from 'react-toastify';
import { clusterApiUrl, PublicKey, SystemProgram } from '@solana/web3.js';
import * as anchor from "@project-serum/anchor";
import { Program, Provider } from "@project-serum/anchor";
import { IDL } from "./../abi/SCSInvestorABI";

import {
  TOKEN_PROGRAM_ID,
  MINT_SIZE,
  createAssociatedTokenAccountInstruction,
  getAssociatedTokenAddress,
  createBurnInstruction,
  createInitializeMintInstruction,
  createTransferInstruction,
  createMintToInstruction,
  getAccount
} from "@solana/spl-token";
const web3 = require("@solana/web3.js"); 
const connection = new web3.Connection(clusterApiUrl(SCSConfig.CLUSTOR as any));
const program_id = new anchor.web3.PublicKey(SCSConfig.PROGRAM_ID)
const idl = IDL;

function SCSHomeVC() { 
  const [provider, setProvider] = useState<any | undefined>(undefined);
  const [walletKey, setWalletKey] = useState<any | undefined>(undefined);
  const [address, setAddress] = useState('');
  const [balance, setBalance] = useState(0);
  const [islogged, setLogged] = useState(false);
  const [type, setType] = useState('');
  const [showModel, setShowModel] = useState(false);
  const [inputValue, setInputValue] = useState('');
  const [inputAddressValue, setInputAddressValue] = useState('');
  const [tx, setTx] = useState('');

  useEffect(() => {
    localStorage.clear()
    const provider = getProvider();
    if (provider) {
      setProvider(provider);
      anchor.setProvider(provider)
    } 
    else setProvider(undefined);
  }, []);

  const loginAction = async () => {
  
    if (provider) {
      try {
        const response:any = await provider.connect();
        console.log("response ",response)
        setWalletKey(response.publicKey.toString());
        setAddress(response.publicKey.toString())
        getTokenBalance(response.publicKey.toString());
        setLogged(true);
      } catch (err:any) {
        toast(err.message)
      }
    }

  };

  const getProvider = (): any | undefined => {
    if ('solana' in window) {
      // @ts-ignore
      const provider = window.solana as any;
      if (provider.isPhantom) return provider as PhantomProvider;
    }
  };

  const showModelAction = (typeValue: string) => {
    setType(typeValue);
    setShowModel(true);
  };

  const submitModelAction = () => {
    setShowModel(false);
    if(type === SCSModelTypesValue.MINT) {
      createSupply(inputValue);
    } else if(type === SCSModelTypesValue.TRANSFER) {
       createTransferAction(inputValue, inputAddressValue)
    } else if(type === SCSModelTypesValue.BURN) {
      createBurnAction(inputValue);
    } else if(type === SCSModelTypesValue.SEND) {
      createPayAction(inputValue);
    }
    setInputAddressValue('');
    setInputValue('');
    setType('');
  };


  const handleInputChange = (event: any) => {
    setInputValue(event.target.value);
  };

  const handleInputAddressChange = (event: any) => {
    setInputAddressValue(event.target.value);
  };

  const logoutAction = async () => {
    if (walletKey && provider) {
      await (provider as PhantomProvider).disconnect();
      setWalletKey(undefined);
      setAddress("")
      setLogged(false);
    }
  }

  const getTokenBalance = async (address:any) => {
     try {
      const mintKey: PublicKey = new PublicKey(SCSConfig.MINT_ADDRESS)
      const key: PublicKey = new PublicKey(address);


      const associatedTokenAccount = await getAssociatedTokenAddress(
        mintKey,
        key
      );

      const myWalletMyTokenBalance = await connection.getTokenAccountBalance(
        associatedTokenAccount
      );
      console.log("myWalletMyTokenBalance ",myWalletMyTokenBalance);
      
      setBalance(myWalletMyTokenBalance.value.uiAmount)
     } catch (error) {
      console.log("myWalletMyTokenBalance error",error);
     }
  }

  const checkTokenAccountExist = async (address:any) => {
    try {
      const mintKey: PublicKey = new PublicKey(SCSConfig.MINT_ADDRESS)
      const key: PublicKey = new PublicKey(address);

      const associatedTokenAccount = await getAssociatedTokenAddress(
        mintKey,
        key
      );
      const myWalletMyTokenBalance = await connection.getTokenAccountBalance(
        associatedTokenAccount
      );
      return true
    } catch (error) {
      return false
    }

  }

  const createSupply = async (supply:any) => {
    try {
      const program = new anchor.Program(idl, program_id);
      const mintPublicKey = new anchor.web3.PublicKey(SCSConfig.MINT_ADDRESS);
      const key = new anchor.web3.PublicKey(address);
  
  
      const associatedTokenAccount = await getAssociatedTokenAddress(
        mintPublicKey,
        key
      );
  
      let transaction = new anchor.web3.Transaction();
      transaction.add(createMintToInstruction(mintPublicKey,associatedTokenAccount,key,supply,[],TOKEN_PROGRAM_ID))
  
      // transaction.add(await program.methods.mintToken(new anchor.BN(supply)).accounts({
      //   mint: mintPublicKey,
      //   tokenProgram: TOKEN_PROGRAM_ID,
      //   tokenAccount: associatedTokenAccount,
      //   authority: key
      // }).signers([]).instruction());

  
      transaction.feePayer = key;
      const anyTransaction = transaction;
      anyTransaction.recentBlockhash = (await connection.getLatestBlockhash()).blockhash;
  
  
      const response:any = await provider.signAndSendTransaction(anyTransaction)
  
      console.log("response ",response)
      setTx(response.signature)
      toast("You have minted successfully");
    } catch (error:any) {
      toast(error.message)
    }



  }



  const createTransferAction = async (supply:any,formAddress:any) => {
     try {
      const program = new anchor.Program(idl, program_id);
      const mintPublicKey = new anchor.web3.PublicKey(SCSConfig.MINT_ADDRESS);
      const sender = new anchor.web3.PublicKey(address);
      const receiver = new anchor.web3.PublicKey(formAddress);

      const associatedTokenAccount = await getAssociatedTokenAddress(
        mintPublicKey,
        sender
      );

      const associatedTokenAccount1 = await getAssociatedTokenAddress(
        mintPublicKey,
        receiver
      );

      const hasTokenAccount:any = await checkTokenAccountExist(receiver)

      let transaction = new anchor.web3.Transaction();

      if(!hasTokenAccount) {
        transaction.add(
          createAssociatedTokenAccountInstruction(
            sender, associatedTokenAccount1, receiver, mintPublicKey
          )
        );
      }

      // transaction.add(await program.methods.transferToken(new anchor.BN(supply)).accounts({
      //   tokenProgram: TOKEN_PROGRAM_ID,
      //   from: associatedTokenAccount,
      //   fromAuthority: sender,
      //   to: associatedTokenAccount1,
      // }).signers([]).instruction());

      transaction.add(createTransferInstruction(associatedTokenAccount,associatedTokenAccount1,sender,supply,[],TOKEN_PROGRAM_ID))

      transaction.feePayer = sender;
      const anyTransaction = transaction;
      anyTransaction.recentBlockhash = (await connection.getLatestBlockhash()).blockhash;
  
      const response:any = await provider.signAndSendTransaction(anyTransaction)
  
      console.log("response ",response)
      setTx(response.signature)
      toast("You have transferred the tokens successfully");

     } catch (error:any) {
      toast(error.message)
     }
  }

  const createBurnAction = async(supply:any)=> {
    try {
      const mintPublicKey = new anchor.web3.PublicKey(SCSConfig.MINT_ADDRESS);
      const key = new anchor.web3.PublicKey(address);
      let transaction = new anchor.web3.Transaction();
      const associatedTokenAccount = await getAssociatedTokenAddress(
        mintPublicKey,
        key
      );
      transaction.add(
        createBurnInstruction(
          associatedTokenAccount, mintPublicKey, key, new anchor.BN(supply),[], TOKEN_PROGRAM_ID
        )
      );
      transaction.feePayer = key;
      const anyTransaction = transaction;
      anyTransaction.recentBlockhash = (await connection.getLatestBlockhash()).blockhash;
  
      const response:any = await provider.signAndSendTransaction(anyTransaction)
  
      console.log("response ",response)
      setTx(response.signature)
      toast("You have burned the tokens successfully");
    } catch (error:any) {
      toast(error.message)
    }
  }

  const createPayAction = async(supply:any)=> {
    try {
      const program = new anchor.Program(idl, program_id);
      const mintPublicKey = new anchor.web3.PublicKey(SCSConfig.MINT_ADDRESS);
      const key = new anchor.web3.PublicKey(address);

      const data = await connection.getTokenLargestAccounts(mintPublicKey);



      const holders = [];
      let totalSupply:any = 0
      for (let index = 0; index < data.value.length; index++) {
        const element = data.value[index];
        const result1 = await getAccount(connection,element.address)
        if(result1.owner.toString() !== address) {
          totalSupply = totalSupply + element.uiAmount;
          holders.push(data.value[index])
        }
      }

      console.log("holders ",holders)
      console.log("totalSupply ",totalSupply)
      let transaction = new anchor.web3.Transaction();
      for (let index = 0; index < holders.length; index++) {
        const element = holders[index];
        console.log(element)
        const result1 = await getAccount(connection,element.address)
        
        if(element.uiAmount > 0) {
          const balance = element.uiAmount
          const amount:any = ((balance/totalSupply) * supply) * 1000000000
          console.log("holdings ", element.uiAmount)
          console.log("amount ", amount);
          // transaction.add(await program.methods.sendRevenue(new anchor.BN(amount)).accounts({
          //   from: key,
          //   to:result1.owner,
          //   systemProgram : SystemProgram.programId
          // }).signers([]).instruction()) ;

          transaction.add(
            SystemProgram.transfer({
              fromPubkey: key,
              toPubkey: result1.owner,
              lamports: parseInt(amount),
            })
          )
        }
      }

      console.log("transaction")
      transaction.feePayer = key;
      const anyTransaction = transaction;
      anyTransaction.recentBlockhash = (await connection.getLatestBlockhash()).blockhash;
  
      const response:any = await provider.signAndSendTransaction(anyTransaction)
  
      console.log("response ",response)
      setTx(response.signature)
      toast("You have burned the tokens successfully");

      
    } catch (error:any) {
      console.log("error ",error)
      toast(error.message)
    }
  }



  return (
    <div className="investmentPage">
      {provider && !islogged && (
        <div className="investorMain">
          <div className="investorMainInner">
            <img src={Logo} alt="scoby dtone" height={80} />
          </div>
          <div className="investorMainInner">
            <button type="button" className="investorBtn" onClick={loginAction}>
              Connect Wallet
            </button>
          </div>
        </div>
      )}

      {!provider && (
        <div className="investorMain">
          <div className="investorMainInner">
            <p>
              No provider found. Install{' '}
              <a href="https://phantom.app/">Phantom Browser extension</a>
            </p>
          </div>
        </div>
      )}

      {islogged && (
        <>
          <div className="header">
            <div className="headerLeft">
              Mint Address:{' '}
              <a
                href={`${SCSConfig.EXPLORER}/address/${SCSConfig.MINT_ADDRESS}?cluster=${SCSConfig.CLUSTOR}`}
                target="_blank"
                rel="noreferrer">
                {SCSConfig.MINT_ADDRESS}
              </a>
              <span className='clustor'>Clustor: {SCSConfig.CLUSTOR}</span>
            </div>
      
            <div className="headerRight">Balance: {balance}</div>
          </div>
          <div className="investorMain">
            <div className="investorMainInner">
              <img src={Logo} alt="scoby dtone" height={80} />

            </div>
            {address &&
                <div className="investorMainInner">
                    <p> Connected Address               <a
                href={`${SCSConfig.EXPLORER}/address/${address}?cluster=${SCSConfig.CLUSTOR}`}
                target="_blank"
                rel="noreferrer">{address}</a></p>
                </div>
             }

            <div className="investorMainInner">
              <button
                type="button"
                className="investorBtn"
                onClick={() => showModelAction(SCSModelTypesValue.MINT)}>
                Mint
              </button>
              <button
                type="button"
                className="investorBtn"
                onClick={() => showModelAction(SCSModelTypesValue.TRANSFER)}>
                Transfer
              </button>
            </div>
            <div className="investorMainInner">
              <button
                type="button"
                className="investorBtn"
                onClick={() => showModelAction(SCSModelTypesValue.BURN)}>
                Burn
              </button>
              <button
                type="button"
                className="investorBtn"
                onClick={() => showModelAction(SCSModelTypesValue.SEND)}>
                Pay
              </button>
            </div>

            <div className="investorMainInner">
              <button
                type="button"
                className="investorBtn"
                onClick={() => logoutAction()}>
                Disconnect
              </button>
            </div>
            {tx!=='' &&
              <div className="investorMainInner">
                    Your last transaction hash is 
                    <a
                  href={`${SCSConfig.EXPLORER}/tx/${tx}?cluster=${SCSConfig.CLUSTOR}`}
                  target="_blank"
                  rel="noreferrer">{tx}</a>
              </div>
            }

          </div>
        </>
      )}
      <Modal
        show={showModel}
        onHide={() => {
          setShowModel(false);
        }}>
        <Modal.Header closeButton>
          {type === SCSModelTypesValue.MINT && <Modal.Title>Mint Token</Modal.Title>}
          {type === SCSModelTypesValue.TRANSFER && <Modal.Title>Transfer Token</Modal.Title>}

          {type === SCSModelTypesValue.BURN && <Modal.Title>Burn Token</Modal.Title>}

          {type === SCSModelTypesValue.SEND && <Modal.Title>Send Token</Modal.Title>}
        </Modal.Header>
        <Modal.Body>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Enter Value</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter Value"
              value={inputValue}
              onChange={handleInputChange}
            />
          </Form.Group>

          {type === SCSModelTypesValue.TRANSFER &&
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Enter Address</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Address"
                  value={inputAddressValue}
                  onChange={handleInputAddressChange}
                />
              </Form.Group>
          }
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => {
              setShowModel(false);
            }}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              submitModelAction();
            }}>
            Submit
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
export default SCSHomeVC;
